/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {

  /**
   * Speed Controller values
   */
  public static final int leftFrontSC = 0;
  public static final int leftRearSC = 1;
  public static final int rightFrontSC = 2;
  public static final int rightRearSC = 3;

  /**
   * Coerce to Range Values
   */
  public static final double C2R_inputMin = -30.0;
  public static final double C2R_inputMax = 30.0;
  public static final double C2R_gyroInputMin = -15.0;
  public static final double C2R_gyroInputMax = 15.0;
  public static final double C2R_outputMin = -0.5;
  public static final double C2R_outputMax = 0.5;
  public static final double C2R_inputWristMin = 510.0;
  public static final double C2R_inputWristMax = 570.0;
  public static final double C2R_outputWristMin = -1.0;
  public static final double C2R_outputWristMax = 1.0;

  public static final double C2RM_inputMin = -27.0; // tx min
  public static final double C2RM_inputMax = 27.0; // tx max
  public static final double C2RM_outputMin = -0.5;
  public static final double C2RM_outputMax = 0.5;

  public static final double S2W_TurnSpeed = 0.45;

  public static final double SonarDistance = 16.5;
}
