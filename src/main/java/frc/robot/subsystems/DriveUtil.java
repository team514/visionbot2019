/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.DriveMecanum;

/**
 * Add your docs here.
 */
public class DriveUtil extends Subsystem {
  SpeedController leftFront, leftRear, rightFront, rightRear;
  MecanumDrive drive;
  ADXRS450_Gyro gyro;

  public boolean arcade = false;

  public DriveUtil() {
    leftFront = new WPI_VictorSPX(RobotMap.leftFrontSC);
    leftRear = new WPI_VictorSPX(RobotMap.leftRearSC);
    rightFront = new WPI_VictorSPX(RobotMap.rightFrontSC);
    rightRear = new WPI_VictorSPX(RobotMap.rightRearSC);

    drive = new MecanumDrive(leftFront, leftRear, rightFront, rightRear);
    drive.setSafetyEnabled(true);

    gyro = new ADXRS450_Gyro(Port.kOnboardCS0);
  }

  public void resetGyro() {
    gyro.reset();
  }

  public void calibrateGyro() {
    gyro.calibrate();
  }

  public double getGyro() {
    return gyro.getAngle();
  }

  public void driveMecanum(double x, double y, double z, boolean useGyro){
		x = squaredInputs(x);
		y = squaredInputs(y);
		z = squaredInputs(z);
		if(useGyro){
			drive.driveCartesian(y, -x, z, getGyro());
		}else{
      drive.driveCartesian(y, -x, z, 0);
		}
	}

  public void driveMecanum(double x, double y, double z){
    x = squaredInputs(x);
    y = squaredInputs(y);
    z = squaredInputs(z);

    drive.driveCartesian(y, -x, z);
  }

  public double squaredInputs(double d){
    boolean negative = d<0;
    d = Math.pow(d, 2);

    if(negative){
      d*= -1;
    }
    
    return d;
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new DriveMecanum());
  }

  public void updateStatus() {
    SmartDashboard.putNumber("gyro angle", getGyro());
  }
} 
