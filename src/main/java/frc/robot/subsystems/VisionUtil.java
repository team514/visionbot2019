/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.limelight.LimeLight;

public class VisionUtil extends Subsystem {
  LimeLight limeLight;
  NetworkTable table;
  NetworkTableEntry tx, ty, ta, ts, tv, ledMode;
  double x, y, area, skew;
  Double valid;
  boolean hasTarget;
  AnalogInput leftSonar, rightSonar;



  public VisionUtil() {
    limeLight = new LimeLight();
    table = NetworkTableInstance.getDefault().getTable("limelight");
    tx = table.getEntry("tx");
    ty = table.getEntry("ty");
    ta = table.getEntry("ta");
    ts = table.getEntry("ts");
    tv = table.getEntry("tv");
    ledMode = table.getEntry("ledMode");
    leftSonar = new AnalogInput(1);
    rightSonar = new AnalogInput(0);

    leftSonar.setAverageBits(1);
    leftSonar.setOversampleBits(1);
    rightSonar.setAverageBits(1);
    rightSonar.setOversampleBits(1);
  }

  public void targetsPeriodic(){
    this.x = tx.getDouble(0.0);
    this.y = ty.getDouble(0.0);
    this.area = ta.getDouble(0.0);
    this.skew = ts.getDouble(0.0);
    this.valid = tv.getNumber(0).doubleValue();
    this.hasTarget = (tv.getNumber(0).doubleValue() == 1.0);
  }

  public double getLeftSonar(){
    double av = leftSonar.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }

  public double getRightSonar(){
    double av = rightSonar.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }

  public void setLEDMode(int mode){ //1 = off 3 = on
    if((mode == 1) || (mode == 3)){
      ledMode.setNumber(mode);
    }
  }
  public void driveMagic(double yAxis, double degrees){ //this is the method to drive straight to target using area
    double adj = coerce2RangeMagic(degrees, RobotMap.C2RM_inputMin, RobotMap.C2RM_inputMax);
		Robot.driveUtil.driveMecanum(adj, yAxis, 0.0, false);
  }

  public void driveHorizontal(double xAxis, double degrees){
    double adj = coerce2RangeMagic(degrees, RobotMap.C2RM_inputMin, RobotMap.C2RM_inputMax);
    Robot.driveUtil.driveMecanum(xAxis, adj, 0.0, false);
  }
  
  
  public double getHorizontalOffset(){
    return this.x;
  }
  
  public double getTargetArea(){
    return this.area;
  }

  public double coerce2RangeMagic(double input, double inputMin, double inputMax){
    double inputCenter;
    double outputMin, outputMax, outputCenter;
    double scale, result;
    //double output;
    
    // inputMin = RobotMap.C2RM_inputMin; 
    // inputMax = RobotMap.C2RM_inputMax;     
    
    outputMin = RobotMap.C2RM_outputMin;
    outputMax = RobotMap.C2RM_outputMax;
    
    /* Determine the center of the input range and output range */
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    scale = (outputMax - outputMin) / (inputMax - inputMin);

    /* Apply the transformation */
    result = (input + -inputCenter) * scale + outputCenter;

    /* Constrain to the output range */
    Math.max(Math.min(result, outputMax), outputMin);

   return Math.max(Math.min(result, outputMax), outputMin);
  }

  @Override
  protected void initDefaultCommand() {
  }

  
  public void updateStatus() {
    SmartDashboard.putNumber("tx = ", this.x);
    SmartDashboard.putNumber("ty = ", this.y);
    SmartDashboard.putNumber("ta = ", this.area);
    SmartDashboard.putNumber("ts = ", this.skew);
    SmartDashboard.putNumber("tv = ", this.valid);
    SmartDashboard.putBoolean("Has Target = ", this.hasTarget);
    SmartDashboard.putNumber("leftSonar", getLeftSonar());
    SmartDashboard.putNumber("rightSonar", getRightSonar());
  }
}
