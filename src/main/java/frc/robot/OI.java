/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.DriveMode;
import frc.robot.commands.SquareToWall;
import frc.robot.commands.lightOff;
import frc.robot.commands.lightOn;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  Joystick leftJoystick, rightJoystick, controller;
  JoystickButton driveMode, driveToTarget, lightOn, lightOff, sonarToWall;

  public OI() {
    leftJoystick = new Joystick(0);
    rightJoystick = new Joystick(1);
    controller = new Joystick(2);
    driveMode = new JoystickButton(rightJoystick, 4);
    lightOn = new JoystickButton(controller, 1);
    lightOff = new JoystickButton(controller, 2);
    sonarToWall = new JoystickButton(controller, 3);
    driveMode.whenPressed(new DriveMode());

    // driveToTarget = new JoystickButton(controller, 3);
    // driveToTarget.whenPressed(new DriveToVisionTarget());

    lightOff.whenPressed(new lightOff());
    lightOn.whenPressed(new lightOn());

    sonarToWall.whenPressed(new SquareToWall());
  }

  public double getLeftX() {
    return leftJoystick.getX();
  }

  public double getLeftY() {
    return leftJoystick.getY();
  }

  public double getLeftZ() {
    return leftJoystick.getZ();
  }

  public double getRightX() {
    return rightJoystick.getX();
  }

  public double getRightY() {
    return rightJoystick.getY();
  }

  public double getRightZ() {
    return rightJoystick.getZ();
  }
}
