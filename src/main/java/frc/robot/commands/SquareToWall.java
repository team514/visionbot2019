/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class SquareToWall extends Command {
  boolean done = false;
  double storedAngle = 1000;

  public SquareToWall() {
    requires(Robot.visionUtil);
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if (storedAngle == 1000) {
      Robot.driveUtil.resetGyro();
      double leftSonar = Robot.visionUtil.getLeftSonar();
      double rightSonar = Robot.visionUtil.getRightSonar();

      double leftRounded = ((int) (leftSonar * 10)) / 10.0;
      double rightRounded = ((int) (rightSonar * 10)) / 10.0;

      System.out.println("left: "+leftSonar+" right: "+rightSonar+" lRound: "+leftRounded +" rRound: " + rightRounded);

      double x = leftRounded;
      double y = rightRounded;

      double angle = Math.toDegrees(Math.atan(RobotMap.SonarDistance / (y - x)));
      double angleRounded = ((int) (angle * 10)) / 10.0;

      storedAngle = angleRounded;
    } else {
      // double gyroValue = Robot.driveUtil.getGyro();
    }

    // System.out.println("DEGREE: " + angle);

    // if (angleRounded == 90 || angleRounded == -90) {
    //   done = true;
    //   return;
    // } else {
    //   done = false;
    // }

    // if (angle > 0) {
    //   Robot.driveUtil.driveMecanum(0, 0, -0.5);
    // } else {
    //   Robot.driveUtil.driveMecanum(0, 0, 0.5);
    // }

    /*if (leftRounded == rightRounded) {
      done = true;
      return;
    } else {
      done = false;
    }

    if(leftRounded > rightRounded){
      Robot.driveUtil.driveMecanum(0.0, 0.0, RobotMap.S2W_TurnSpeed);
    }else{
      Robot.driveUtil.driveMecanum(0.0, 0.0, -RobotMap.S2W_TurnSpeed);
    }*/
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
